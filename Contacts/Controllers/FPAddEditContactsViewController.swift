//
//  FPAddEditContactsViewController.swift
//  Contacts
//
//  Created by Subham Khandelwal on 02/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit

enum cellTypes {
    case unknown
    case simpleTextField
    case addRow
    case deleteRow
}

struct cellDetails {
    
    var cellType : cellTypes
    var textFieldValue = ""
    var fieldPlaceHolder = ""
}

class FPAddEditContactsViewController : FPBaseViewController, UITableViewDelegate, UITableViewDataSource, FPChangeRowCellDelegate {
    
    let simpleTextFieldCellId = "FPSimpleTextFieldCell"
    let addRowCellId = "FPAddRowCell"
    let deleteRowCell = "FPDeleteRowCell"
    var isFavorite = false
    
    var contactDetailsArr = [[cellDetails]]()
    var savedContactDetails : ContactDetails?
    var tableHeight : CGFloat = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
        self.configuireContactModel()
    }
    
    func configuireContactModel() {
        
        let firstNameFieldDetails = cellDetails(cellType: .simpleTextField, textFieldValue: savedContactDetails?.firstName ?? "", fieldPlaceHolder:"First Name")
        let lastNameFieldDetails = cellDetails(cellType: .simpleTextField, textFieldValue: savedContactDetails?.lastName ?? "", fieldPlaceHolder: "Last Name")
        let companyFieldDetails = cellDetails(cellType: .simpleTextField, textFieldValue: savedContactDetails?.company ?? "", fieldPlaceHolder: "Company")
        
        let firstSection = [firstNameFieldDetails, lastNameFieldDetails, companyFieldDetails]
        contactDetailsArr.append(firstSection)
        
        var phoneFields = [cellDetails]()
        if let savedContact = savedContactDetails {
            for phone in savedContact.phoneNumberArr {
                phoneFields.append(cellDetails(cellType: .deleteRow, textFieldValue: phone, fieldPlaceHolder: "Phone"))
            }
        }
        var addRowFieldDetails = cellDetails(cellType: .addRow, textFieldValue: "Add Phone", fieldPlaceHolder: "Add Phone")
        phoneFields.append(addRowFieldDetails)
        contactDetailsArr.append(phoneFields)
        
        var emailFields = [cellDetails]()
        if let savedContact = savedContactDetails {
            for email in savedContact.emailArr {
                emailFields.append(cellDetails(cellType: .deleteRow, textFieldValue: email, fieldPlaceHolder: "Email"))
            }
        }
        addRowFieldDetails = cellDetails(cellType: .addRow, textFieldValue: "Add Email", fieldPlaceHolder: "Add Email")
        emailFields.append(addRowFieldDetails)
        contactDetailsArr.append(emailFields)
        
        if savedContactDetails != nil {
            isFavorite = (savedContactDetails?.isFavorites)!
        } else {
            isFavorite = false
        }
    }
    
    func configureUI() {
        
        let doneBtn = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneBtnTapped))
        self.navigationItem.rightBarButtonItem = doneBtn

        tableView.register(UINib(nibName: "FPSimpleTextFieldCell", bundle: nil), forCellReuseIdentifier: simpleTextFieldCellId)
        tableView.register(UINib(nibName: "FPAddRowCell", bundle: nil), forCellReuseIdentifier: addRowCellId)
        tableView.register(UINib(nibName: "FPDeleteRowCell", bundle: nil), forCellReuseIdentifier: deleteRowCell)
        tableHeight = tableHeightConstraint.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
            name: NSNotification.Name.UIKeyboardWillShow, object: nil
        )
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: NSNotification.Name.UIKeyboardWillHide, object: nil
        )
    }
    
    
    // TableView Delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if let savedContact = savedContactDetails, savedContact.id > 0 {
            return contactDetailsArr.count + 2
        } else {
            return contactDetailsArr.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        var height = 0.0
        height = section == 0 ? 0.0 : 50.0
        return CGFloat(height)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == contactDetailsArr.count {
            // Add to favorite view
            let addToFavContactView = UIButton(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width-20, height: 50))
            addToFavContactView.setTitle(isFavorite ? "Remove from favorite" : "Add to favorite", for: .normal)
            addToFavContactView.setTitleColor(UIColor.black, for: .normal)
            addToFavContactView.titleLabel?.textAlignment = .center
            addToFavContactView.addTarget(self, action: #selector(addToFavTapped), for: .touchUpInside)
            return addToFavContactView
        } else if section == contactDetailsArr.count + 1 {
            
            // Delete contact view
            let deleteContactView = UILabel(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.size.width-20, height: 50))
            deleteContactView.text = "Delete Contact"
            deleteContactView.textColor = UIColor.red
            deleteContactView.textAlignment = .center
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(deleteContactTapped))
            deleteContactView.addGestureRecognizer(tapGesture)
            deleteContactView.isUserInteractionEnabled = true
            return deleteContactView
        } else {
            return UIView(frame: CGRect(x: 0, y: 0, width: 1, height: 50))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section >= contactDetailsArr.count {
            return 0
        } else {
            return contactDetailsArr[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let currentSection = contactDetailsArr[indexPath.section]
        let currentRow : cellDetails = currentSection[indexPath.row]
            switch currentRow.cellType {
            case .simpleTextField:
                let cell : FPSimpleTextFieldCell = tableView.dequeueReusableCell(withIdentifier: simpleTextFieldCellId) as! FPSimpleTextFieldCell
                cell.section = indexPath.section
                cell.row = indexPath.row
                cell.delegate = self
                cell.textField.placeholder = currentRow.fieldPlaceHolder
                cell.textField.text = currentRow.textFieldValue
                cell.selectionStyle = .none
                
                return cell
            case .addRow:
                let cell : FPAddRowCell = tableView.dequeueReusableCell(withIdentifier: addRowCellId) as! FPAddRowCell
                cell.addCellLabel.text = currentRow.textFieldValue
                return cell
            case .deleteRow:
                let cell : FPDeleteRowCell = tableView.dequeueReusableCell(withIdentifier: deleteRowCell) as! FPDeleteRowCell
                cell.delegate = self
                cell.section = indexPath.section
                cell.row = indexPath.row
                cell.textField.placeholder = currentRow.fieldPlaceHolder
                cell.textField.text = currentRow.textFieldValue
                cell.selectionStyle = .none
                
                return cell
            case .unknown:
                let cell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
                return cell
            }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentSectionDetails : [cellDetails] = contactDetailsArr[indexPath.section]
        if (currentSectionDetails[indexPath.row].cellType == .addRow) {
            self.addRowCalled(section: indexPath.section, row: indexPath.row)
        }
    }

    func changeInTextField(section : Int, row : Int, text : String) {
        var currentSectionDetails : [cellDetails] = contactDetailsArr[section]
        var currentRowDetails : cellDetails = currentSectionDetails[row]
        currentRowDetails.textFieldValue = text
        currentSectionDetails[row] = currentRowDetails
        contactDetailsArr[section] = currentSectionDetails
    }
    
    func addRowCalled(section : Int, row : Int) {
        var placeholderText = ""
        switch section {
        case 1:
            placeholderText = "Phone"
        case 2:
            placeholderText = "Email"
        default:
            placeholderText = ""
        }
        var currentSectionDetails : [cellDetails] = contactDetailsArr[section]
        currentSectionDetails.insert(cellDetails(cellType: .deleteRow, textFieldValue: "", fieldPlaceHolder: placeholderText), at: row)
        contactDetailsArr[section] = currentSectionDetails
        tableView.reloadData()
    }
    
    func deleteRowCalled(section : Int, row : Int) {
        var currentSectionDetails : [cellDetails] = contactDetailsArr[section]
        currentSectionDetails.remove(at: row)
        contactDetailsArr[section] = currentSectionDetails
        tableView.reloadData()
    }
    
    @objc func doneBtnTapped() {
        let basicDetails : [cellDetails] = contactDetailsArr[0]
        let firstName = basicDetails[0].textFieldValue
        let lastName = basicDetails[1].textFieldValue
        let company = basicDetails[2].textFieldValue

        var mobileArr = [String]()
        if contactDetailsArr[1].count > 1 {
            for index in 0...contactDetailsArr[1].count-2 {
                let val = contactDetailsArr[1][index].textFieldValue
                if val.isPhone() {
                    mobileArr.append(val)
                }
            }
        }

        var emailArr = [String]()
        if contactDetailsArr[2].count > 1 {
            for index in 0...contactDetailsArr[2].count-2 {
                let val = contactDetailsArr[2][index].textFieldValue
                if val.isEmailValid() {
                    emailArr.append(val)
                }
            }
        }
        
        if firstName.isEmpty && lastName.isEmpty && company.isEmpty && mobileArr.isEmpty && emailArr.isEmpty {
            FPHelperClass.showAlertOnVC(parentVC: self, title: "", subtitle: "Please fill atleast one value")
            return;
        }
        
        var currentContactId : Int?
        if let contactId = savedContactDetails?.id {
            FPHelperClass.deleteContact(contactId)
            currentContactId = contactId
        } else {
            currentContactId = FPHelperClass.getContactIdToSave()
        }
        let contactDetails = ContactDetails(id: currentContactId!, firstName: firstName, lastName: lastName, company: company, phoneNumberArr: mobileArr, emailArr: emailArr, isFavorites: isFavorite)
        FPHelperClass.saveContact(newContact: contactDetails)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @objc func deleteContactTapped() {
        FPHelperClass.deleteContact((savedContactDetails?.id)!)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func addToFavTapped() {
        isFavorite = !isFavorite
        self.doneBtnTapped()
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            tableHeightConstraint.constant = tableHeight-keyboardHeight
        }
    }
    
    @objc func keyboardWillHide() {
        tableHeightConstraint.constant = tableHeight
    }
}
