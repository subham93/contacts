//
//  ViewController.swift
//  Contacts
//
//  Created by Subham Khandelwal on 01/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import UIKit

class FPContactsViewController: FPBaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var contactsTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let reuseId = "allContactsId"
    var userContacts = [[ContactDetails]]()
    var filteredContacts = [ContactDetails]()
    var searchActive : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getAllContacts()
    }
    
    func configureUI() {
        let addBtn = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addContacts))
        let favBtn = UIBarButtonItem(title: "Favorite", style: .done, target: self, action: #selector(viewFavorites))
        self.navigationItem.rightBarButtonItems = [favBtn, addBtn]
        
        contactsTableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseId)
        contactsTableView.tableFooterView = UIView()
    }
    
    func getAllContacts() {
        userContacts.removeAll()
        
        let contacts : [ContactDetails]? = FPHelperClass.getContacts()
        if var savedContacts = contacts, savedContacts.count > 0 {
            let str = "abcdefghijklmnopqrstuvwxyz#"
            
            savedContacts = savedContacts.sorted(by: { (a : ContactDetails, b : ContactDetails) -> Bool in
                return a.firstName?.caseInsensitiveCompare(b.firstName!) == .orderedAscending
            })
            for prefix in str {
                var eachSectionArr = [ContactDetails]()
                for contact in savedContacts {
                    
                    // Form contact list sorted and sectionally
                    if !(contact.firstName?.isEmpty)! {
                        if (contact.firstName?.lowercased().hasPrefix(String(prefix)))! {
                            eachSectionArr.append(contact)
                        }
                    } else if !(contact.lastName?.isEmpty)! {
                        if (contact.lastName?.lowercased().hasPrefix(String(prefix)))! {
                            eachSectionArr.append(contact)
                        }
                    } else if String(prefix) == "#" {
                        eachSectionArr.append(contact)
                    }
                }
                if eachSectionArr.count > 0 {
                    userContacts.append(eachSectionArr)
                }
            }
        }
        
        contactsTableView.reloadData()
    }
    
    @objc func addContacts() {
        let addVC = FPHelperClass.getStoryboard().instantiateViewController(withIdentifier: "FPAddEditContactsViewController")
        self.navigationController?.pushViewController(addVC, animated: true)
    }
    
    @objc func viewFavorites() {
        let favVC = FPHelperClass.getStoryboard().instantiateViewController(withIdentifier: "FPFavoriteViewController")
        self.navigationController?.pushViewController(favVC, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        searchBar.endEditing(true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var sections = 0
        sections = searchActive ? 1 : userContacts.count
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        rows = searchActive ? filteredContacts.count : userContacts[section].count
        return rows
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var headerTitle = ""
        if (searchActive) {
            return ""
        } else {
            if userContacts[section].count > 0 {
                if let firstName = userContacts[section][0].firstName, !firstName.isEmpty {
                    headerTitle = String(firstName.prefix(1)).uppercased()
                } else if let lastName = userContacts[section][0].lastName, !lastName.isEmpty {
                    headerTitle = String(lastName.prefix(1)).uppercased()
                } else {
                    headerTitle = "#"
                }
            }
            return headerTitle
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let contactDetails = searchActive ? filteredContacts[indexPath.row] : userContacts[indexPath.section][indexPath.row]
        
        let cell : UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: reuseId))!
        let firstName = (contactDetails.firstName != nil) ? contactDetails.firstName! : ""
        let lastName = (contactDetails.lastName != nil) ? contactDetails.lastName! : ""
        let fullName = firstName + " " + lastName
        var displayName = ""
        if !firstName.isEmpty || !lastName.isEmpty {
            displayName = fullName
        } else if contactDetails.phoneNumberArr.count > 0 {
            displayName = contactDetails.phoneNumberArr[0]
        } else if contactDetails.emailArr.count > 0 {
            displayName = contactDetails.emailArr[0]
        }
        cell.textLabel?.text = displayName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let contactDetails = searchActive ? filteredContacts[indexPath.row] : userContacts[indexPath.section][indexPath.row]

        tableView.deselectRow(at: indexPath, animated: true)
        let viewContactVC = FPHelperClass.getStoryboard().instantiateViewController(withIdentifier: "FPViewContactsViewController") as! FPViewContactsViewController
        viewContactVC.contactDetails = contactDetails
        self.navigationController?.pushViewController(viewContactVC, animated: true)
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if searchActive {
            return []
        }
        
        var headerTitleArr = [String]()
        let flattenedArr = userContacts.flatMap { $0 }
        for contactDetails in flattenedArr {
            if let firstName = contactDetails.firstName, !firstName.isEmpty {
                if headerTitleArr.contains(String(firstName.prefix(1))) == false {
                    headerTitleArr.append(String(firstName.prefix(1)).uppercased())
                }
            } else if let lastName = contactDetails.lastName, !lastName.isEmpty {
                if headerTitleArr.contains(String(lastName.prefix(1))) == false {
                    headerTitleArr.append(String(lastName.prefix(1)).uppercased())
                }
            } else {
                if headerTitleArr.contains("#") == false {
                    headerTitleArr.append("#")
                }
            }
        }
        return headerTitleArr
    }
    
    
    
    // Search Bar
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.isEmpty {
            searchActive = false
            contactsTableView.reloadData()
            return
        }
        let flattenedArr = userContacts.flatMap { $0 }
        filteredContacts = FPHelperClass.getFilteredContacts(with: searchText, from: flattenedArr)
        searchActive = true
        contactsTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
