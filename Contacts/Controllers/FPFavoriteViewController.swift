//
//  FPFavoriteViewController.swift
//  Contacts
//
//  Created by Subham Khandelwal on 05/04/18.
//  Copyright © 2018 FPTech. All rights reserved.
//

import Foundation
import UIKit

class FPFavoriteViewController : FPBaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    let reuseId = "cellID"
    
    var favoriteContacts : [ContactDetails] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configuireUI()
        favoriteContacts = FPHelperClass.getAllFavoriteContacts()!
    }
    
    func configuireUI() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseId)
        tableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteContacts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let contactDetails = favoriteContacts[indexPath.row]
        let cell : UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: reuseId))!
        let firstName = (contactDetails.firstName != nil) ? contactDetails.firstName! : ""
        let lastName = (contactDetails.lastName != nil) ? contactDetails.lastName! : ""
        let fullName = firstName + " " + lastName
        var displayName = ""
        if !firstName.isEmpty || !lastName.isEmpty {
            displayName = fullName
        } else if contactDetails.phoneNumberArr.count > 0 {
            displayName = contactDetails.phoneNumberArr[0]
        } else if contactDetails.emailArr.count > 0 {
            displayName = contactDetails.emailArr[0]
        }
        cell.textLabel?.text = displayName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let viewContactVC = FPHelperClass.getStoryboard().instantiateViewController(withIdentifier: "FPViewContactsViewController") as! FPViewContactsViewController
        viewContactVC.contactDetails = favoriteContacts[indexPath.row]
        self.navigationController?.pushViewController(viewContactVC, animated: true)

    }
}
