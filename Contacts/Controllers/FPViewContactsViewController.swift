//
//  FPViewContactsViewController.swift
//  Contacts
//
//  Created by Subham Khandelwal on 04/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit



class FPViewContactsViewController : FPBaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    enum DataType {
        
        case basicDetils
        case phones
        case emails
    }
    let reuseId = "reuseId"
    var contactDetails : ContactDetails?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configuireUI()
    }
    
    func configuireUI() {
        let editBtn = UIBarButtonItem(title: "Edit", style: .done, target: self, action: #selector(editBtnTapped))
        self.navigationItem.rightBarButtonItem = editBtn

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseId)
        tableView.tableFooterView = UIView()
    }
    
    @objc func editBtnTapped() {
        let addVC = FPHelperClass.getStoryboard().instantiateViewController(withIdentifier: "FPAddEditContactsViewController") as! FPAddEditContactsViewController
        addVC.savedContactDetails = contactDetails
        self.navigationController?.pushViewController(addVC, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case DataType.basicDetils.hashValue:
            return 150
        case DataType.phones.hashValue, DataType.emails.hashValue:
            return 30
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case DataType.basicDetils.hashValue:
            let headerView = FPViewContactsHeaderView(frame : CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width-20, height: 150)) // FPViewContactsHeaderView(coder: nil)
            headerView.companyLbl.text = contactDetails?.company
            
            let firstName = (contactDetails?.firstName != nil) ? contactDetails?.firstName! : ""
            let lastName = (contactDetails?.lastName != nil) ? contactDetails?.lastName! : ""
            headerView.nameLbl.text = firstName! + " " + lastName!
            
            if let name = contactDetails?.firstName {
                headerView.initialLabel.text = String(name.prefix(1)).uppercased()
            }
            return headerView
        case DataType.phones.hashValue:
            let titleLbl = UILabel(frame: CGRect(x: 0, y: 0, width: 1, height: 30))
            titleLbl.text = "Phone(s):"
            titleLbl.backgroundColor = UIColor.lightGray
            return titleLbl
        case DataType.emails.hashValue:
            let titleLbl = UILabel(frame: CGRect(x: 0, y: 0, width: 1, height: 30))
            titleLbl.text = "Email(s):"
            titleLbl.backgroundColor = UIColor.lightGray
            return titleLbl
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let whiteView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        whiteView.backgroundColor = UIColor.white
        return whiteView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case DataType.phones.hashValue:
            return (contactDetails?.phoneNumberArr.count)!
        case DataType.emails.hashValue:
            return (contactDetails?.emailArr.count)!
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseId)!
        switch indexPath.section {
        case DataType.phones.hashValue:
            cell.textLabel?.text = contactDetails?.phoneNumberArr[indexPath.row]
        case DataType.emails.hashValue:
            cell.textLabel?.text = contactDetails?.emailArr[indexPath.row]
        default:
            cell.textLabel?.text = ""
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}

