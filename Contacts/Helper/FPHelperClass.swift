//
//  FPHelperClass.swift
//  Contacts
//
//  Created by Subham Khandelwal on 02/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit

class FPHelperClass {
    
    static let kSavedContacts = "savedContacts"
    
    class func getContacts() -> [ContactDetails] {
        if let contactsData = UserDefaults.standard.data(forKey: kSavedContacts),
            let contacts : [ContactDetails] = try? JSONDecoder().decode([ContactDetails].self, from: contactsData) as [ContactDetails] {
            return contacts
        }
        return [ContactDetails]()
    }
    
    class func saveContact(newContact : ContactDetails) {
        var contacts = self.getContacts()
        contacts.append(newContact)
        self.saveAllContacts(contacts)
    }
    
    class func saveAllContacts(_ allContacts : [ContactDetails]?) {
        if let encoded = try? JSONEncoder().encode(allContacts) {
            UserDefaults.standard.set(encoded, forKey: kSavedContacts)
            UserDefaults.standard.synchronize()
        } else {
            print("Error encoding::saveAllContacts")
        }
    }
    
    class func deleteContact(_ contactId : Int) {
        let contacts = self.getContacts()
        let newContactsArr = contacts.filter({ (eachContact : ContactDetails) -> Bool in
            return eachContact.id != contactId
        })
        self.saveAllContacts(newContactsArr)
    }
    
    class func getContactById(_ contactId : Int) -> ContactDetails? {
        let contacts = self.getContacts()
        let contactById : [ContactDetails] = contacts.filter({ (eachContact : ContactDetails) -> Bool in
            return eachContact.id == contactId
        })
        return contactById.count > 0 ? contactById[0] : nil
    }
    
    class func getAllFavoriteContacts() -> [ContactDetails]? {
        let contacts = self.getContacts()
        let favContacts : [ContactDetails] = contacts.filter({ (eachContact : ContactDetails) -> Bool in
            return eachContact.isFavorites
        })
        return favContacts
    }
    
    class func getContactIdToSave() -> Int {
        let contacts : [ContactDetails] = self.getContacts()
        if contacts.count == 0 {
            return 1;
        } else {
            return contacts[contacts.count-1].id + 1
        }
    }
    
    class func getFilteredContacts(with searchText : String, from flattenedArr : [ContactDetails]) -> [ContactDetails] {
        let filteredArr = flattenedArr.filter({ (contact : ContactDetails) -> Bool in
            var isFound : Bool = false
            if !(contact.firstName?.isEmpty)! {
                let range = contact.firstName?.range(of: searchText, options: .caseInsensitive)
                isFound = range != nil ? true : false
            } else if !(contact.lastName?.isEmpty)! {
                let range = contact.lastName?.range(of: searchText, options: .caseInsensitive)
                isFound = range != nil ? true : false
            }
            if !(contact.phoneNumberArr.isEmpty) {
                for eachContact in contact.phoneNumberArr {
                    let range = eachContact.range(of: searchText, options: .caseInsensitive)
                    if range != nil {
                        return true
                    }
                }
            }
            if !(contact.firstName?.isEmpty)! && !(contact.lastName?.isEmpty)! {
                let fullName = contact.firstName! + " " + contact.lastName!
                let range = fullName.range(of: searchText, options: .caseInsensitive)
                isFound = range != nil ? true : false
            }
            return isFound
        })

        return filteredArr
    }
    
    class func getStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    class func showAlertOnVC(parentVC : UIViewController, title : String, subtitle : String) {
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        parentVC.present(alert, animated: true, completion: nil)
    }
}



extension String {
    
    func isEmailValid() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isPhone() -> Bool {
        return  !self.isEmpty && CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: self))
    }
}
