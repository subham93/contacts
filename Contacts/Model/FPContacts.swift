//
//  FPContacts.swift
//  Contacts
//
//  Created by Subham Khandelwal on 02/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation

struct ContactDetails : Codable {
    var id : Int
    var firstName : String?
    var lastName : String?
    var company : String?
    var phoneNumberArr : [String] = []
    var emailArr : [String] = []
    var isFavorites : Bool
}
