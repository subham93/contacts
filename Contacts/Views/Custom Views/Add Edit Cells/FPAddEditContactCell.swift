//
//  FPAddEditContactCell.swift
//  Contacts
//
//  Created by Subham Khandelwal on 03/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit

protocol FPChangeRowCellDelegate {
    func deleteRowCalled(section : Int, row : Int)
    func changeInTextField(section : Int, row : Int, text : String)
    func dismissKeyboard()
}

class FPAddEditContactCell : UITableViewCell, UITextFieldDelegate {
    
    var delegate : FPChangeRowCellDelegate? = nil
    var section : Int?
    var row : Int?
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        delegate?.changeInTextField(section: section!, row: row!, text: newString)
        return true
    }
    
    func dismissKeyboard() {
        delegate?.dismissKeyboard()
    }
}
