//
//  FPDeleteRowCell.swift
//  Contacts
//
//  Created by Subham Khandelwal on 02/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit

class FPDeleteRowCell : FPAddEditContactCell {
    
    @IBOutlet weak var textField: UITextField!

    @IBAction func minusButtonTapped(_ sender: Any) {
        delegate?.deleteRowCalled(section: section!, row: row!)
    }
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        super.dismissKeyboard()
        return true
    }
}
