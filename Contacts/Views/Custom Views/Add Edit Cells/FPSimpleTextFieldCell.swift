//
//  FPSimpleTextFieldCell.swift
//  Contacts
//
//  Created by Subham Khandelwal on 02/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit

class FPSimpleTextFieldCell : FPAddEditContactCell {
    
    @IBOutlet weak var textField: FPCustomTextField!
    
    func textFieldShouldReturn(_ scoreText: UITextField) -> Bool {
        super.dismissKeyboard()
        return true
    }
}
