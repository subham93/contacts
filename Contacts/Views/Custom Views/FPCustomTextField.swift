//
//  FPCustomTextField.swift
//  Contacts
//
//  Created by Subham Khandelwal on 02/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit

class FPCustomTextField : UITextField {
    
    var customDelegate: UITextFieldDelegate?
    
    override init(frame: CGRect) {
        super.init(frame : frame)
        self.setBottomBorder()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder);
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setBottomBorder()
    }
    
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
