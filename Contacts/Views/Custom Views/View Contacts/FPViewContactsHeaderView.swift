//
//  FPViewContactsHeaderView.swift
//  Contacts
//
//  Created by Subham Khandelwal on 04/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import Foundation
import UIKit

class FPViewContactsHeaderView : UIView {
    
    @IBOutlet weak var initialLabel: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var companyLbl: UILabel!
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.xibSetup()
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView? {
        let className = String(describing: type(of: self)).components(separatedBy: ".").last!
        let nibName = UINib(nibName:className, bundle:nil)
        return nibName.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
}
