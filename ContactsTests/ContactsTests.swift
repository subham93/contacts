//
//  ContactsTests.swift
//  ContactsTests
//
//  Created by Subham Khandelwal on 01/04/18.
//  Copyright © 2018 Contacts. All rights reserved.
//

import XCTest
@testable import Contacts

class ContactsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetContacts() {
        
        // Check if contacts is nil
        if FPHelperClass.getContacts() != nil {
            XCTAssert(true, "Success")
        } else {
            XCTAssert(false, "Failed")
        }
    }
    
    func saveContact() -> Int {
        let newId = FPHelperClass.getContactIdToSave()
        FPHelperClass.saveContact(newContact: ContactDetails(id: newId, firstName: "Test First Name", lastName: "Test Last Name", company: "Test Company", phoneNumberArr: ["123", "456"], emailArr: ["abc@asd.com", "xasxas@asd.com", "asdxw@as.com"], isFavorites: false))
        return newId
    }
    
    func testSaveContacts() {
        let newId = self.saveContact()
        let contactDetails : ContactDetails? = FPHelperClass.getContactById(newId)
        (contactDetails?.firstName == "Test First Name") ? XCTAssert(true, "Success") : XCTAssert(false, "Failed")
    }
    
    func testSaveAtoZContacts() {
        let str = "abcdefghijklmnopqrstuvwxyz"
        for prefix in str {
            let newId = FPHelperClass.getContactIdToSave()
            FPHelperClass.saveContact(newContact: ContactDetails(id: newId, firstName: String(prefix), lastName: String(prefix), company: String(prefix) + "Test Company", phoneNumberArr: ["123", "456"], emailArr: ["abc@asd.com", "xasxas@asd.com", "asdxw@as.com"], isFavorites: false))
        }
    }
    
    func testFavotiteContact() {
        let newId = FPHelperClass.getContactIdToSave()
        FPHelperClass.saveContact(newContact: ContactDetails(id: newId, firstName: "Test First Name", lastName: "Test Last Name", company: "Test Company", phoneNumberArr: ["123", "456"], emailArr: ["abc@asd.com", "xasxas@asd.com", "asdxw@as.com"], isFavorites: true))
        let contactDetails : ContactDetails? = FPHelperClass.getContactById(newId)
        (contactDetails?.isFavorites)! ? XCTAssert(true, "Success") : XCTAssert(false, "Failed")
    }
    
    func testDeleteContacts() {
        let newId = self.saveContact()
        FPHelperClass.deleteContact(newId)
        FPHelperClass.getContactById(newId) == nil ? XCTAssert(true, "Success") : XCTAssert(false, "Failed")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
